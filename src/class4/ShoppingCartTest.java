package class4;

import static org.junit.Assert.*;

import org.junit.Test;

public class ShoppingCartTest {

	//testcase1: Creating a new cart has 0 items
	@Test
	public void testCreateNewCart() {
		
		//1 Create a shopping cart
		ShoppingCart cart = new ShoppingCart();
		//2 check how many items are in it
		int numItems = cart.getItemCount();
		//3 Compare your actual with expected
		
		assertEquals(0,numItems);
		//if(numItems==0)
		//{System.out.println("	pass	")}
	}
	//Test case2: An Empty cart has 0 items.
	@Test
	public void testEmptyCart()
	{
		   
		
	}
	//testcase3 : 
	@Test
	public void testAddItem()
	{
		ShoppingCart cart = new ShoppingCart();
		cart.empty();
		cart.addItem(new Product("Phone",2000));
		int numItems = cart.getItemCount();
		assertEquals(1,numItems);
	}
}
